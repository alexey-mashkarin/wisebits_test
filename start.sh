#!/bin/sh

php /var/www/html/artisan migrate --seed --no-interaction

php /var/www/html/artisan cache:clear --no-interaction
php /var/www/html/artisan config:clear --no-interaction
php /var/www/html/artisan view:clear --no-interaction
php /var/www/html/artisan event:clear --no-interaction
php /var/www/html/artisan route:clear --no-interaction

/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf