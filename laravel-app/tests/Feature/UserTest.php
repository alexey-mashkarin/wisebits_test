<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserChangeHistory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;


class UserTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCRUD()
    {

        // test create

        $usersTable = 'users';
        $user = DB::query()
            ->from($usersTable)
            ->limit(1)
            ->get()
            ->first()
        ;
        $this->assertEmpty($user);


        $userData = [
            'name' => 'testcrud',
            'email' => $this->faker->email(),
            'notes' => $this->faker->paragraph(),
        ];
        $userModel = new User($userData);
        $userModel->save();

        $user = DB::query()
            ->from($usersTable)
            ->limit(1)
            ->get()
            ->first()
        ;
        $this->assertNotEmpty($user);

        foreach ($userData as $field => $value) {
            $this->assertEquals($user->$field, $value);
        }


        // test write
        $userId = $user->id;
        $this->assertIsInt($userId);

        $userModel = User::find($userId);
        $this->assertInstanceOf(User::class, $userModel);
        foreach ($userData as $field => $value) {
            $this->assertEquals($user->$field, $value);
        }


        // test update
        $newUserData = [
            'name' => 'testcrudupdated',
            'email' => $this->faker->email(),
            'notes' => $this->faker->paragraph(),
        ];
        $userModel->fill($newUserData);
        $userModel->save();

        $user = DB::query()
            ->from($usersTable)
            ->where('id', $userId)
            ->limit(1)
            ->get()
            ->first()
        ;
        foreach ($newUserData as $field => $value) {
            $this->assertEquals($user->$field, $value);
        }


        // test delete
        $userModel->delete();
        $user = DB::query()
            ->from($usersTable)
            ->where('id', $userId)
            ->limit(1)
            ->get()
            ->first()
        ;
        $this->assertNotNull($user);
        $this->assertNotNull($user->deleted);


        // test force delete
        $userModel->forceDelete();
        $user = DB::query()
            ->from($usersTable)
            ->where('id', $userId)
            ->limit(1)
            ->get()
            ->first()
        ;
        $this->assertNull($user);
    }

    public function testValidation()
    {
        $userValidData = [
            'name' => 'testValidation',
            'email' => $this->faker->email(),
        ];


        $userModel = new User($userValidData);
        $userModel->name = '';

        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name field is required.');
        }

        $userModel->name = '1';
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name must be at least 8 characters.');
        }


        $userModel->name = str_repeat('u', 257);
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name must not be greater than 256 characters.');
        }


        $userModel->name = 'invalid user name';
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name format is invalid.');
        }


        $userModel->name = 'bullshit'; // здесь надо бы замокать словарь
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name contains swear word.');
        }


        $userModel->name = 'correctusername';
        try {
            $userModel->save();
        } catch (\Exception $exception) {
            $this->assertTrue(false, 'Unexpected exception: ' . $exception->getMessage());
        }


        $userModel2 = new User($userValidData);
        $userModel2->name = $userModel->name;
        try {
            $userModel2->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('name', $validationException->errors());
            $this->assertEquals($validationException->errors()['name'][0], 'The name has already been taken.');
        }


        $userModel->email = '';
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('email', $validationException->errors());
            $this->assertEquals($validationException->errors()['email'][0], 'The email field is required.');
        }


        $userModel->email = 'incorrect_email';
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException, $validationException->getMessage());
            $this->assertArrayHasKey('email', $validationException->errors());
            $this->assertEquals($validationException->errors()['email'][0], 'The email must be a valid email address.');
        }


        $userModel->email = 'test@xxx.com'; // словарь доменов надо бы замокать
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException, $validationException->getMessage());
            $this->assertArrayHasKey('email', $validationException->errors());
            $this->assertEquals($validationException->errors()['email'][0], 'The email has insecure domain.');
        }

        $userModel->email = $userValidData['email'];
        try {
            $userModel->save();
        } catch (\Exception $exception) {
            $this->assertTrue(false, 'Unexpected exception: ' . $exception->getMessage());
        }


        $userModel2->email = $userModel->email;
        try {
            $userModel2->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException, $validationException->getMessage());
            $this->assertArrayHasKey('email', $validationException->errors());
            $this->assertEquals($validationException->errors()['email'][0], 'The email has already been taken.');
        }
    }

    public function testDeleted()
    {
        $usersTable = 'users';
        $userData = [
            'name' => 'testdeleted',
            'email' =>  $this->faker->email(),
        ];

        $userModel = new User($userData);
        $userModel->save();

        $user = DB::query()
            ->from($usersTable)
            ->where('id', $userModel->id)
            ->limit(1)
            ->get()
            ->first()
        ;

        $this->assertNotNull($user);
        $this->assertNull($user->deleted);


        $userModel->delete();
        $user = DB::query()
            ->from($usersTable)
            ->where('id', $userModel->id)
            ->limit(1)
            ->get()
            ->first()
        ;
        $this->assertNotNull($user);
        $this->assertNotNull($user->deleted);


        $userModel->deleted = $userModel->created;
        try {
            $userModel->save();
            $this->assertFalse(true, 'User validation dose`t work properly');
        } catch (\Exception $validationException) {
            $this->assertInstanceOf(ValidationException::class, $validationException);

            $this->assertArrayHasKey('deleted', $validationException->errors());
            $this->assertEquals($validationException->errors()['deleted'][0], 'The "deleted" must be greater than "created"');
        }
    }

    public function testHistory()
    {
        $userData = [
            'name' => 'testhistory',
            'email' =>  $this->faker->email(),
        ];

        $userModel = new User($userData);
        $userModel->save();

        $userHistory = UserChangeHistory::query()
            ->where('user_id', $userModel->id)
            ->get()
        ;
        $this->assertCount(0, $userHistory);


        $userChangedData = [
            'name' => 'testhistory2',
            'email' =>  $this->faker->email(),
            'notes' => $this->faker->paragraph(),
        ];
        $userModel->fill($userChangedData);
        $userModel->save();

        $userHistory = UserChangeHistory::query()
            ->where('user_id', $userModel->id)
            ->get()
        ;
        $this->assertCount(1, $userHistory);

        $userHistoryModel = $userHistory->first();
        $this->assertEquals($userData, $userHistoryModel->old_value);
        $this->assertEquals($userChangedData, $userHistoryModel->new_value);
    }
}
