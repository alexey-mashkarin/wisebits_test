<?php

namespace Tests\Unit;

use App\Services\SwearWordsDictionaryService;
use PHPUnit\Framework\TestCase;

class SwearWordsDictionaryServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDictionary()
    {
        $swearWordsDictionaryService = new SwearWordsDictionaryService();
        $arDictionary = $swearWordsDictionaryService->getDictionary();
        $this->assertNotEmpty($arDictionary);
        $this->assertIsArray($arDictionary);
    }


    public function testIsStringContainsSwearWords()
    {
        $arTestDictionary = [
            'ass',
            'hell',
        ];
        $mockedService = $this->createPartialMock(SwearWordsDictionaryService::class, ['getDictionary']);
        $mockedService->method('getDictionary')->willReturn($arTestDictionary);

        $this->assertTrue($mockedService->isStringContainsSwearWords('ASS'));
        $this->assertTrue($mockedService->isStringContainsSwearWords('hell'));
        $this->assertTrue($mockedService->isStringContainsSwearWords('ass hole'));
        $this->assertTrue($mockedService->isStringContainsSwearWords('kiss my ass'));
        $this->assertTrue($mockedService->isStringContainsSwearWords('welcome to hell!!!'));

        $this->assertFalse($mockedService->isStringContainsSwearWords('passage'));
        $this->assertFalse($mockedService->isStringContainsSwearWords('HELLO'));
    }
}
