<?php

namespace Tests\Unit;

use App\Services\InsecureDomainsDictionaryService;
use PHPUnit\Framework\TestCase;

class InsecureDomainsDictionaryServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDictionary()
    {
        $swearWordsDictionaryService = new InsecureDomainsDictionaryService();
        $arDictionary = $swearWordsDictionaryService->getDictionary();
        $this->assertNotEmpty($arDictionary);
        $this->assertIsArray($arDictionary);
    }


    public function testIsStringContainsSwearWords()
    {
        $arTestDictionary = [
            'local',
            'insecure.com',
        ];
        $mockedService = $this->createPartialMock(InsecureDomainsDictionaryService::class, ['getDictionary']);
        $mockedService->method('getDictionary')->willReturn($arTestDictionary);

        $this->assertTrue($mockedService->isEmailWithInsecureDomain('test@local'));
        $this->assertTrue($mockedService->isEmailWithInsecureDomain('test@insecure.com'));


        $this->assertFalse($mockedService->isEmailWithInsecureDomain('test@secure.com'));
        $this->assertFalse($mockedService->isEmailWithInsecureDomain('test@local.domain'));

        $this->expectException(\Exception::class);
        $mockedService->isEmailWithInsecureDomain('invalid_email_string');
    }
}
