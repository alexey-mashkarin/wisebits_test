<?php

namespace App\Services;


class InsecureDomainsDictionaryService
{
    public function getDictionary(): array
    {
        return [
            'local',
            'xxx.com',
            'xyz.com',
        ];
    }

    public function isEmailWithInsecureDomain(string $email): bool
    {
        $atSignPosition = strrchr($email, '@');
        if ($atSignPosition === false) {
            throw new \Exception('Email string is incorrect');
        }

        $emailDomain = substr($atSignPosition, 1);
        $dictionaryIndex = array_flip($this->getDictionary());

        return isset($dictionaryIndex[$emailDomain]);
    }
}
