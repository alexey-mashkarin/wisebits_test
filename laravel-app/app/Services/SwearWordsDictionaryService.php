<?php

namespace App\Services;


class SwearWordsDictionaryService
{
    public function getDictionary(): array
    {
        return [
            'ass',
            'asshole',
            'bitch',
            'bullshit',
            'damn',
            'fuck',
            'nigga',
            'shit',
        ];
    }

    public function isStringContainsSwearWords(string $string): bool
    {
        $pattern = '/\b(' . implode('|', $this->getDictionary()) . ')\b/i';
        return preg_match($pattern, $string);
    }
}
