<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;

class UserChangeHistory extends Model
{
    use HasTimestamps;

    const CREATED_AT = 'created';
    const UPDATED_AT = null;

    protected $fillable = [
        'user_id',
        'new_value',
        'old_value',
    ];

    protected $casts = [
        'new_value' => 'array',
        'old_value' => 'array',
    ];
}
