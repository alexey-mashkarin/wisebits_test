<?php

namespace App\Models;

use App\Services\InsecureDomainsDictionaryService;
use App\Services\SwearWordsDictionaryService;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class User extends Model
{
    use SoftDeletes, HasTimestamps;


    const DELETED_AT = 'deleted';
    const CREATED_AT = 'created';
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'notes',
    ];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saving(function (User $user) {

            $rules = [
                'name' => [
                    'required',
                    'max:256',
                    'min:8',
                    'regex:/^[a-z0-9]+$/',
                    function ($attribute, $value, $fail) {
                        $swearWordsDictionaryService = resolve(SwearWordsDictionaryService::class);
                        /**
                         * @var SwearWordsDictionaryService $swearWordsDictionaryService
                         */

                        if ($swearWordsDictionaryService->isStringContainsSwearWords($value)) {
                            $fail('The ' . $attribute . ' contains swear word.');
                        }
                    },
                ],
                'email' => [
                    'required',
                    'email:rfc',
                    function ($attribute, $value, $fail) {
                        $insecureDomainsDictionaryService = resolve(InsecureDomainsDictionaryService::class);
                        /**
                         * @var InsecureDomainsDictionaryService $insecureDomainsDictionaryService
                         */

                        try {
                            if ($insecureDomainsDictionaryService->isEmailWithInsecureDomain($value)) {
                                $fail('The ' . $attribute . ' has insecure domain.');
                            }
                        } catch (\Exception $exception) {
                            $fail($exception->getMessage());
                        }
                    },
                ],
                static::DELETED_AT => [
                    'nullable',
                    function ($attribute, $value, $fail) use ($user) {

                        if ($value <= $user->{static::CREATED_AT}) {
                            $fail('The "' . $attribute . '" must be greater than "' . static::CREATED_AT . '"');
                        }
                    },
                ]
            ];

            $dataForChange = $user->getDirty();
            if (isset($dataForChange['name'])) {
                $rules['name'][] = 'unique:users';
            }
            if (isset($dataForChange['email'])) {
                $rules['email'][] = 'unique:users';
            }


            $validator = Validator::make($user->getAttributes(), $rules);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
        });

        static::updated(function (User $user) {
            $historyData = [
                'user_id' => $user->id,
                'new_value' => $user->getDirty(),
                'old_value' => array_intersect_key($user->getOriginal(), $user->getDirty()),
            ];
            (new UserChangeHistory($historyData))->save();
        });

    }
}
