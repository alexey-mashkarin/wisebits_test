FROM node:17.3.0-alpine3.15 as build-stage
WORKDIR /app
COPY ./laravel-app /app
RUN set -x \
&& apk add yarn \
&& yarn install \
&& npm run production


FROM registry.gitlab.com/alexey-mashkarin/nginx-php-fpm:latest

COPY ./default-site.conf /etc/nginx/conf.d/default-site.conf

WORKDIR /var/www/html

RUN set -x \
&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
&& php composer-setup.php \
&& mv composer.phar /usr/local/bin/composer \
&& php -r "unlink('composer-setup.php');"


COPY ./start.sh /usr/local/bin/start.sh
COPY --chown=nginx:nginx ./laravel-app /var/www/html
RUN set -x \
&& cd /var/www/html \
&& composer install \
&& chmod +x /usr/local/bin/start.sh

COPY --from=build-stage --chown=nginx:nginx /app/public /var/www/html/public

CMD /usr/local/bin/start.sh