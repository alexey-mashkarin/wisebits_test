#!/bin/sh
if [ -n "$CREATE_TEST_DB" ]; then

  if [ -z "$TEST_DB" ]; then
    MYSQL_TEST_DB='test_db'
  fi

  mysql_note "Creating database ${MYSQL_TEST_DB}"
  docker_process_sql --database=mysql <<<"CREATE DATABASE IF NOT EXISTS \`$MYSQL_TEST_DB\` ;"

  if [ -n "$MYSQL_USER" ]; then
      mysql_note "Giving user ${MYSQL_USER} access to schema ${MYSQL_TEST_DB}"
      docker_process_sql --database=mysql <<<"GRANT ALL ON \`${MYSQL_TEST_DB//_/\\_}\`.* TO '$MYSQL_USER'@'%' ;"
  else
    mysql_note "You should create user for ${MYSQL_TEST_DB} manually"
  fi

fi
